import { Component, OnInit } from '@angular/core';
import { UserService } from '../../shared/user.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { routerTransition } from '../../router.animations';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {
    isLoginError = false;
    constructor(private userService: UserService, private router: Router) { }

    ngOnInit() {
        if (localStorage.getItem('userToken') != null) {
            this.router.navigate(['/dashboard']);
        }
    }

    OnSubmit(userName, password) {
        this.userService.userAuthentication(userName, password).subscribe((data: any) => {
            localStorage.setItem('userToken', data.access_token);
            this.router.navigate(['/dashboard']);
        }, (err: HttpErrorResponse) => {
            this.isLoginError = true;
        });
    }
}