export class Roles {
    administrator?: boolean;
    editor?: boolean;
}

export class User {
    UserName: string;
    Password: string;
    Email: string;
    FirstName: string;
    LastName: string;
    roles: Roles;
}
