import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';

@Injectable()
export class UserService {
    readonly rootUrl = 'http://localhost:8765';

    constructor(private http: HttpClient) {
    }

    userAuthentication(userName, password) {
        const data = 'username=' + userName + '&password=' + password + '&grant_type=password';
        const reqHeader = new HttpHeaders({'Content-Type': 'application/x-www-urlencoded', 'No-Auth': 'True'});
        return this.http.post(this.rootUrl + '/Token', data, {headers: reqHeader});
    }

    getUserClaims() {
        return this.http.get(this.rootUrl + '/GetUserClaims');
    }
}