// import { BrowserModule } from '@angular/platform-browser';
// import { NgModule } from '@angular/core';
// import { FormsModule} from '@angular/forms';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
//
// import { AppComponent } from './app.component';
// import { UserService } from './shared/user.service';
// import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
// import { UserComponent } from './user/user.component';
// import { SignInComponent } from './user/sign-in/sign-in.component';
// import { DashboardComponent } from './dashboard/dashboard.component';
// import { MoviesComponent } from './movies/movies.component';
// import { AppRoutingModule } from './app-routing.module';
// import { AuthGuard } from './auth/auth.guard';
// import { AuthInterceptor } from './auth/auth.interceptor';
//
// @NgModule({
//   declarations: [
//     AppComponent,
//     UserComponent,
//     SignInComponent,
//     DashboardComponent,
//     MoviesComponent
//   ],
//   imports: [
//     BrowserModule,
//     FormsModule,
//     HttpClientModule,
//     BrowserAnimationsModule,
//     AppRoutingModule
//   ],
//   providers: [UserService, AuthGuard, {
//       provide : HTTP_INTERCEPTORS,
//       useClass : AuthInterceptor,
//       multi : true
//     }],
//   bootstrap: [AppComponent]
// })
// export class AppModule { }
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule} from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthGuard } from './shared';
import { UserService } from './shared/user.service';
import { AuthInterceptor } from './shared/guard/auth.interceptor';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
// AoT requires an exported function for factories
export function createTranslateLoader(http: HttpClient) {
    // for development
    // return new TranslateHttpLoader(http, '/start-angular/SB-Admin-BS4-Angular-5/master/dist/assets/i18n/', '.json');
    // return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        FormsModule,
        BrowserAnimationsModule,
        HttpClientModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            }
        }),
        AppRoutingModule
    ],
    declarations: [AppComponent],
    providers: [UserService, AuthGuard, {
      provide : HTTP_INTERCEPTORS,
      useClass : AuthInterceptor,
      multi : true
    }],
    bootstrap: [AppComponent]
})
export class AppModule {}